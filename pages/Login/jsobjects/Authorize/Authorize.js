export default {
	async myFun2 () {
		await DoAuth.run();
		if (DoAuth.data.login === InputLogin.text && DoAuth.data.password === InputPassword.text){
			await GetOperateToken.run();
			await GetTasklistToken.run();
			if (GetOperateToken.data && GetTasklistToken.data){
				storeValue('operate_token', GetOperateToken.data.access_token)
				storeValue('tasklist_token', GetTasklistToken.data.access_token)
				storeValue('backBaseUrl', 'http://192.168.0.10:8098')
				storeValue('fileServiceBaseUrl', 'http://192.168.0.10:8482')
				storeValue('positionId', DoAuth.data.positionId)
				storeValue('loggedIn', true)
				storeValue('isLogin', true);
			  storeValue('userRole', DoAuth.data.role);
				storeValue('userLogin', InputLogin.text)
				storeValue('userFullName', DoAuth.data.firstName + ' ' + DoAuth.data.lastName)
				if (appsmith.store.loggedIn) navigateTo('Applications');
			}
		} else {
			showAlert('Перевірте дані та повторіть спробу')
		}
	}
}