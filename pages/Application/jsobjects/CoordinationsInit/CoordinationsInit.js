export default {

	async setupInterviewDate () {
		await GetApplicationVariables.data;

		let coordinationObj = {};
		coordinationObj.interviewerId = ApplicationInit.coordinationList.filter(x => x.positionId === SelectInterviewer.selectedOptionValue)[0].employeeId;
		coordinationObj.interviewDate = DatePickerInterviewDate.selectedDate;

		let interviewDate = GetApplicationVariables.data.filter(x => x.name === 'interviewDate')[0];
		if (interviewDate === null || interviewDate === undefined){
			await SaveInterviewDate.run({coordinationObj: coordinationObj});
		} else {
			await SaveInterviewDateCopy.run({coordinationObj: coordinationObj});
		}

		await Tasks.completeTask();
		if (appsmith.store.loggedIn) await navigateTo('Applications');
	},	
	async setupInterviewStatus () {
		await GetApplicationVariables.data;
		
		let coordinationObj = {};
		coordinationObj.interviewStatusId = SelectInterviewResult.selectedOptionValue;
		coordinationObj.intervewComment =  InputInterviewComment.text;
		coordinationObj.interviewerId =  ApplicationInit.coordinationList.filter(x => x.positionId === SelectInterviewer.selectedOptionValue)[0].employeeId;
		
		let interviewResult = GetApplicationVariables.data.filter(x => x.name === 'interviewResult')[0];
		if (interviewResult === null || interviewResult === undefined){
			await SaveInterviewResult.run({coordinationObj: coordinationObj});
		} else {
			await SaveInterviewResultCopy.run({coordinationObj: coordinationObj});
		}

		await Tasks.completeTask();
		if (appsmith.store.loggedIn) await navigateTo('Applications');
	}
}