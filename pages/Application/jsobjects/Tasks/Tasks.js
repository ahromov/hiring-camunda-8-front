export default {
	
	async completeTask () {
		await GetTasks.run({procId: appsmith.URL.queryParams.appId});
		await CompleteTask.run({ taskId: GetTasks.data[0].id });
		await navigateTo('Applications');
	},
	async toApprove () {
		await SaveVariable.run({"state":"На погодженні"});
		await Tasks.completeTask();
	}
}