export default {

	appVars: [],
	stateNew: '',
	applicationNumber: '',
	applicationCreateDate: '',
	applicationType: '',
	initiatorName: '',
	coordinationList: [],
	photoId: '',
	assignee: '',
	state: '',
	offerFileId: '',
	interviewConclusions: [],
	interview1Date: null,
	interview2Date: null,
	isSaved: false,

	async onload () {
		if (appsmith.URL.queryParams.appId !== undefined){
			await GetApplicationVariables.run()
				.then(() =>  {
				if (GetApplicationVariables.data.filter(x => x.name === 'appVariables') !== undefined && GetApplicationVariables.data.filter(x => x.name === 'appVariables').length > 0) this.appVars = JSON.parse(GetApplicationVariables.data.filter(x => x.name === 'appVariables')[0].value)
			})
				.then(() =>  {
				if (GetApplicationVariables.data.filter(x => x.name === 'appDocuments') !== undefined && GetApplicationVariables.data.filter(x => x.name === 'appDocuments').length > 0) FilesInit.docs = JSON.parse(GetApplicationVariables.data.filter(x => x.name === 'appDocuments')[0].value)
			})
				.then(() => this.stateNew = GetApplicationVariables.data.filter(x => x.name === 'stateNew')[0].value)
				.then(() => this.applicationNumber = GetApplicationVariables.data.filter(x => x.name === 'applicationNumber')[0].value)
				.then(() => this.applicationCreateDate = GetApplicationVariables.data.filter(x => x.name === 'applicationCreateDate')[0].value)
				.then(() => this.applicationType = GetApplicationVariables.data.filter(x => x.name === 'applicationType')[0].value)
				.then(() => this.initiatorName = GetApplicationVariables.data.filter(x => x.name === 'initiatorName')[0].value)
				.then(() => {
				if (GetApplicationVariables.data.filter(x => x.name === 'coordinationList').length > 0) 
					this.coordinationList = JSON.parse(GetApplicationVariables.data.filter(x => x.name === 'coordinationList')[0].value)
			})
				.then(() =>  GetApplicationVariables.data.filter(x => x.name === 'state').length > 0 ? this.state = GetApplicationVariables.data.filter(x => x.name === 'state')[0].value.replaceAll('"', '') : '')
				.then(() => GetApplicationVariables.data.filter(x => x.name === 'offerFileId').length > 0 ? this.offerFileId = GetApplicationVariables.data.filter(x => x.name === 'offerFileId')[0].value.replaceAll('"', '') : '')
				.then(() => GetApplicationVariables.data.filter(x => x.name === 'interviewConclusions').length > 0 ? this.interviewConclusions = JSON.parse(GetApplicationVariables.data.filter(x => x.name === 'interviewConclusions')[0].value).interviewConclusions : [])
				.then(() => {
				let interviewDate = GetApplicationVariables.data.filter(x => x.name === 'interviewDate');
				interviewDate.length > 0? this.interview1Date = JSON.parse(interviewDate[0].value) : null
			})
				.then(() => {
				let interviewDate = GetApplicationVariables.data.filter(x => x.name === 'interviewDate2');
				interviewDate.length > 0? this.interview2Date = JSON.parse(interviewDate[0].value) : null
			});

			await GetDepartments.run();
			await GetSubdivisionByDepId.run();
			await GetPositionsBySubdivId.run()
			await GetAppTypes.run();
			if (this.appVars.photoData !== undefined) await GetFile.run(() =>{}, () =>{}, { id: this.appVars.photoData });
			await Coordinations.run(() => {}, () => {}, { depId: SelectDepartement.selectedOptionValue, subId: SelectSection.selectedOptionValue })
				.then(() => this.coordinationList = Coordinations.data.coordinationList);
		}		
	},
	async onSave () {
		await SaveGeneralVariables.run();
		await SaveGeneralDocuments.run();
		// await ApplicationInit.onload();
		await showAlert('Дані успішно збережені', 'success');
		this.isSaved = (InputAppCandidateSurNameUk.text &&     
										InputAppCandidateSurNameEn.text &&     
										InputAppCandidateFirstNameUk.text &&   
										InputAppCandidateFirstNameEn.text &&   
										InputAppCandidateLastNameUk.text &&    
										InputAppCandidateLastNameEn.text &&    
										InputAppCandidateRegularAddres.text && 
										InputAppCandidateRegAddr.text &&       
										InputAppCandidatePhone.text &&         
										InputAppCandidateEmail.text &&         
										InputAppContactName.text &&            
										InputAppContactState.text &&           
										InputAppContactPhone.text &&           
										InputTempSalary.text &&                
										InputRegSalary.text &&
										CheckboxPassport.isChecked &&           
										InputPasspostSeries.text &&             
										InputPassportNumber.text &&             
										InputPassportModeratorIssue.text &&     
										DatePickerPassportIssue.selectedDate && 
										InputCitizenship.text &&                
										CheckboxRNKOPP.isChecked &&             
										InputRNKOPPNumber.text &&               
										DatePickerRNKOPPValidUntil.selectedDate 
									 );
	},
	async initCoordinations () {
		await Coordinations.run(() => {}, () => {}, { depId: SelectDepartement.selectedOptionValue, subId: SelectSection.selectedOptionValue })
			.then(() => this.coordinationList = Coordinations.data.coordinationList);
	},
	async appCancel () {
		await SaveVariable.run({"state": "Відхилена"})
		await CancelProcess.run();
		setInterval(async () => {
			await GetProcessess.run();		
			let proc = GetProcessess.data.processInstances.find(x => x.id === appsmith.URL.queryParams.appId && x.state === 'CANCELED').processId;
			if (proc !== undefined){
				if (appsmith.store.loggedIn) navigateTo('Applications');
			}
		}, 2000)
	},
	async onPhotoUpload () {
		if (this.appVars.photoData !== null) DeleteFile.run({id: this.appVars.photoData})
		await UploadFile.run(() => {}, () => {}, { files: FilePickerAppCandidatePhoto.files })
			.then(() => this.photoId = UploadFile.data.id);
		await UpdateFile.run(() => {}, () => {}, { id: UploadFile.data.id });
		await GetFile.run(() => {}, () => {}, { id: UploadFile.data.id });			
	},
	applicationNumberInit () {
		return this.applicationNumber;
	},
	applicationCreateDateInit () {
		return this.applicationCreateDate;
	},
	applicationTypeInit () {
		return this.applicationType;
	},
	initiatorNameInit () {
		return this.initiatorName;
	},
	initDefaultDepartments () {
		return  this.appVars.departmentName !== undefined ? this.appVars.departmentName[0].value : 1;
	},
	initDefaultSubdivision () {
		return this.appVars.subdivisionName !== undefined ? this.appVars.subdivisionName[0].value : GetSubdivisionByDepId.data[0].id;
	},
	initDefaultPositions () {
		return this.appVars.positionName !== undefined ? this.appVars.positionName[0].value : GetPositionsBySubdivId.data[0].id;
	},	
	initBirthDate () {
		return this.appVars.birthday ? this.appVars.birthday : '2023-01-01T00:00:00'
	},
	initHiringDate () {
		return this.appVars.hiringDate ? this.appVars.hiringDate : '2023-01-01T00:00:00'
	},
	initInterviewDate () {
		if (this.interview2Date != null) return this.interview2Date.interviewDate
		else if (this.interview1Date != null) return this.interview1Date.interviewDate
		else return '2023-12-30T10:30:23.310Z'
	},
	initDefaultAppType () {
		return this.appVars.employmentType !== undefined ? this.appVars.employmentType[0].value : GetAppTypes.data[0].id;
	},
	getPhotoId () {
		if (this.photoId === '') {
			return this.appVars.photoData;
		} else {
			return this.photoId;
		}
	},
	photoInit () {
		if (GetFile.data !== null) return atob(GetFile.data);
	},
	initCoordination1Name () {
		return this.coordinationList.length > 0 ? this.coordinationList[0].name : '';
	},
	initCoordination1Position () {
		return this.coordinationList.length > 0 ? this.coordinationList[0].position : '';
	},
	initCoordination1Priority () {
		return this.coordinationList.length > 0 ? this.coordinationList[0].priority : '';
	},
	initCoordination2Name () {
		return this.coordinationList.length > 0 ? this.coordinationList[1].name : '';
	},
	initCoordination2Position () {
		return this.coordinationList.length > 0 ? this.coordinationList[1].position : '';
	},
	initCoordination2Priority () {
		return this.coordinationList.length > 0 ? this.coordinationList[1].priority : '';
	},
	disable() {
		return appsmith.store.positionId === 1 && appsmith.URL.queryParams.roleId === '1' ;
	},
	getConclusions () {
		return this.interviewConclusions.length > 0 ? this.interviewConclusions : [];
	},
	async onExit () {
		await storeValue('loggedIn', false)
		await navigateTo('Applications', {}, 'SAME_WINDOW');
	},
	async isInSession () {
		if (!appsmith.store.isLogin) navigateTo('Login');
	}
}	
