export default {

	passportFileId: '',
	idCardFileId: '',
	taxFileId: '',
	orderFileId: '',
	docs: [],
	fileList: [],


	async onPassportUpload () {
		if (this.docs.passport !== undefined && this.docs.passport.file_id !== null) DeleteFile.run({id: this.docs.passport.file_id})
		await UploadFile.run(() => {}, () => {}, { files: FilePicker1.files })
			.then(async () => { await UpdateFile.run(() => {}, () => {}, { id: UploadFile.data.id }) } )
			.then(() => {this.passportFileId = UploadFile.data.id});
	},
	async onidCardFileIdUpload () {
		if (this.docs.id_card !== undefined && this.docs.id_card.file_id !== null) DeleteFile.run({id: this.docs.id_card.file_id})
		await UploadFile.run(() => {}, () => {}, { files: FilePicker1Copy.files })
			.then(async () => { await UpdateFile.run(() => {}, () => {}, { id: UploadFile.data.id }) } )
			.then(() => {this.idCardFileId = UploadFile.data.id});
	},
	async onidTaxUpload () {
		if (this.docs.rnkopp !== undefined && this.docs.rnkopp.file_id !== null) DeleteFile.run({id: this.docs.rnkopp.file_id})
		await UploadFile.run(() => {}, () => {}, { files: FilePicker1CopyCopy.files })
			.then(async () => { await UpdateFile.run(() => {}, () => {}, { id: UploadFile.data.id }) } )
			.then(() => {this.taxFileId = UploadFile.data.id});
	},
	async onOrderUpload () {
		if (this.docs.order !== undefined && this.docs.order.file_id !== null) DeleteFile.run({id: this.docs.order.file_id})
		await UploadFile.run(() => {}, () => {}, { files: FilePicker1CopyCopyCopy.files })
			.then(async () => { await UpdateFile.run(() => {}, () => {}, { id: UploadFile.data.id }) } )
			.then(() => {this.orderFileId = UploadFile.data.id});
	},
	async onOtherFileUplod () {
		if ((this.docs.other !== undefined && this.docs.other.files !== undefined) && this.docs.other.files.length > 0) {
			this.fileList = this.docs.other.files;
			this.docs.other.files = [];
		}
		await UploadFile.run(() => {}, () => {}, { files: FilePicker2.files })
			.then(async () => { await UpdateFile.run({ id: UploadFile.data.id }) } )
			.then(async () => await this.fileList.push({ "id": UploadFile.data.id, "name": UploadFile.data.originalName }));
	},
	getPassportFileId () {
		if (this.passportFileId === '') {
			return this.docs.passport.file_id;
		} else {
			return this.passportFileId;
		}
	},
	getIdCardFileId () {
		if (this.idCardFileId === '') {
			return this.docs.id_card.file_id;
		} else {
			return this.idCardFileId;
		}
	},
	getRnkoppFileId () {
		if (this.taxFileId === '') {
			return this.docs.rnkopp.file_id;
		} else {
			return this.taxFileId;
		}
	},
	getOrderFileId () {
		if (this.orderFileId === '') {
			return this.docs.order.file_id;
		} else {
			return this.orderFileId;
		}
	},
	otherFilesInit () {
		if (this.fileList.length > 0){
			return this.fileList;
		} else {
			return  this.docs.other.files;
		}
	},
	async downloadPassportFile () {
		if (this.docs.passport.file_id !== '') {
			await DownloadFile.run({id: this.docs.passport.file_id});
			download(atob(DownloadFile.data.buffer), DownloadFile.data.originalName, DownloadFile.data.mimetype);
		} 
	},
	async downloadidCardFile () {
		if (this.docs.id_card.file_id !== '') {
			await DownloadFile.run({id: this.docs.id_card.file_id});
			download(atob(DownloadFile.data.buffer), DownloadFile.data.originalName, DownloadFile.data.mimetype);
		} 
	},
	async downloadidTaxFile () {
		if (this.docs.rnkopp.file_id !== '') {
			await DownloadFile.run({id: this.docs.rnkopp.file_id});
			download(atob(DownloadFile.data.buffer), DownloadFile.data.originalName, DownloadFile.data.mimetype);
		} 
	},
	async downloadidOrderFile () {
		if (this.docs.order.file_id !== '') {
			await DownloadFile.run({id: this.docs.order.file_id});
			download(atob(DownloadFile.data.buffer), DownloadFile.data.originalName, DownloadFile.data.mimetype);
		}
	},
	async downloadOtherFile(id) {
		await DownloadFile.run({id: id});
		download(atob(DownloadFile.data.buffer), DownloadFile.data.originalName, DownloadFile.data.mimetype);
	},
	async deleteFile(id) {
		// await DeleteFile.run({id: id});
		this.fileList = this.docs.other.files.filter(x => x.id !== id);
	},
	async onOfferDownload () {
		if (ApplicationInit.offerFileId !== '') {
			await DownloadFile.run({id: ApplicationInit.offerFileId});
			await download(atob(DownloadFile.data.buffer), DownloadFile.data.originalName, DownloadFile.data.mimetype);
		}
	}
}