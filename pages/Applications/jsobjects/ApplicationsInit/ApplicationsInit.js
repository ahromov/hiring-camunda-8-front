export default {
	
	initiator: 'HR',
	type: 'Прийом',
	
	async getApplications () {
		await GetProcessess.run();
	},
	async show () {
		if (TableApplicationsList.selectedRow.state === 'В процесі'){
			setInterval(async () => {
				await GetTask.run(() => {}, () => {}, { procId: TableApplicationsList.selectedRow.id })
					.then(() => { 
					if (GetTask.data[0].processInstanceKey !== undefined && GetTask.data[0].processInstanceKey == Number.parseInt(TableApplicationsList.selectedRow.id)) { 
						navigateTo('Application', { "appId": GetTask.data[0].processInstanceKey, "appStep": GetTask.data[0].name, "roleId": GetTask.data[0].assignee });
					}	
				});
			}, 2000)	
		} else {
			navigateTo('Application', { "appId": Number.parseInt(TableApplicationsList.selectedRow.id), "appStep": "", "roleId": appsmith.store.positionId });
		}
	},
	async start () {
		await StartProcess.run()
			.then(() => setInterval(async () => {
			await GetTask.run(() => {},() => {}, { procId: StartProcess.data.id });
			if (GetTask.data[0].processInstanceKey !== undefined && GetTask.data[0].processInstanceKey == StartProcess.data.processInstanceKey) { 
				navigateTo('Application', { "appId": GetTask.data[0].processInstanceKey, "appStep": GetTask.data[0].name, "roleId": GetTask.data[0].assignee });
			}	
		}, 2000));
	},
	showAll () {
		return GetProcessess.data.processInstances.filter(x => x.processName === 'hiring-demo');
	},
	async delete (id) {
		await DeleteProcess.run(() => {}, () => {}, { procId: id});
		await showAlert('Запит відправлено успішно', 'success')
		await GetProcessess.run();		
	},
	async exit() {
		storeValue('isLogin', false)
		clearStore()
		navigateTo('Login', {}, 'SAME_WINDOW');
	},
	showState (state) {
		switch(state){
			case 'CANCELED': return 'Відхилена';
			case 'COMPLETED': return 'Завершена';
			case 'ACTIVE': return 'В процесі';
			case 'INCIDENT': return 'В інциденті';	
		}
	},
	async isInSession () {
		if (!appsmith.store.isLogin) navigateTo('Login');
	}
}
